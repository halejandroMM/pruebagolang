/*
 *  api
 *
 *  @author Alejandro Mendoza Moreno <halejandromendozamoreno@gmail.com>
 *
 *  January 2020
 */
package api

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"prueba_go/model/domain"
	"prueba_go/model/server"
	"prueba_go/model/ssl"
	"prueba_go/model/website"
	"prueba_go/model/whois"
	"sync"
	"time"
	"errors"
	"strings"

	"github.com/go-chi/chi"
	"golang.org/x/net/html"
)

var wg sync.WaitGroup

// GetDataDomainHandler nos permite manejar la petición a la ruta '/domain/{domainName}' y retornar
//la información asociada a un dominio accediendo a dierentes fuentes de información
func GetDataDomainHandler(w http.ResponseWriter, r *http.Request) {
	//Se recibe el parametro domainName que contiene el dominio a consultar
	domainName := chi.URLParam(r, "domainName")

	if domainName != "" {

		//Se establece el puntero de tipo dominio que se empleara con receiver para acceder a los métodos
		domainVar := new(domain.Domain)

		//Se consulta el dominio para saber si ya se ha registrado su consulta antes.
		//Si no existe, quiere decir que es primera vez que se consulta, por tanto se registra el dominio
		// y sus servidores, se sabe que estos no han cambiado y su ssl_previous es igual al actual.

		//Si el dominio existe ya se requiere hacer un update de este registro para ingresar la fecha de consulta
		//y los demás atributos que requieran actualización.
		//Por otra parte, existen servidores asociados al dominio, por lo que se debe validar si estos cambiaron.
		//De ser así, se debe actualizar los servidores a su estado previous y registrar los nuevos como current.
		domainVar.GetByName(domainName)

		//Se establecen 3 gorutinas en el waitgroup, una para el titulo, otra para el logo  y la ultima para el
		// análisis ssl del dominio

		wg.Add(1)

		//Se establece 1 gorutina para la obtención de los servers del dominio

		sslDomain := ssl.SslResponse{}
		go func(domain string) {
			defer wg.Done()

			result, err := GetSslResponse(domain)
			if err != nil {
				http.Error(w, "El dominio no ha podido ser accedido para obtener el análisis ssl, verificar si se ingreso correctamente, si cuenta con conexión a internet e intentar de nuevo.", http.StatusBadRequest)
				return
			}
			sslDomain =  result

		}(domainName)

		//Obtención del title, logo y status del dominio

		// Se realiza la petición al dominio para obtener el Response y despues realizar el parser Html
		// Al realizar la petición se puede determinar si esta down o no.
		// Si se encuentra ok se procede a la extracción del logo y el title con gorutinas para optimizar
		// el proceso.
		status := false
		title := ""
		logo := ""

		resp, err := http.Get("http://" + domainName)

		if err != nil {
			status = true
			http.Error(w, "No se pudo acceder al dominio, revisa que esta correctamente ingresado o tu conexión a internet.", http.StatusBadRequest)
			return
		}
		if resp != nil {
			defer resp.Body.Close()
		}

		wg.Add(2)
		doc, err := html.Parse(resp.Body)
		logoChannel := make(chan string)  // Declare a unbuffered channel para la obtención del logo
		titleChannel := make(chan string) // Declare a unbuffered channel para la obtención del titulo

		if err != nil {
			status = true
		} else {

			go website.GetLogo(doc, logoChannel, &wg)
			logo = <-logoChannel // Obtención de la ruta del logo

			go website.GetTitle(doc, titleChannel, &wg)
			title = <-titleChannel // Obtención del titulo

		}

		wg.Wait()

		//Se debe esperar a que se obtenga la informacion ssl para proceder a obtener la información
		//de los servidores y hacer el llamado a whois.
		close(logoChannel)  // Cerrar el channel
		close(titleChannel) // Cerrar el channel

		wg.Add(len(sslDomain.Endpoints)) // Se adicionan las gorutinas para procesar la cantidad de servers

		// Declare a unbuffered channel para la obtención de data de un server

		serverList := []server.Server{} // Lista de servers despues de ejecutar whois

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel() // Se debe asegurar de llamarlo para liberar recursos incluso si no hay errores

		for _, serverItem := range sslDomain.Endpoints {
			serverVar := server.Server{}
			serverVar.Address = serverItem.IPAddress
			//Se debe validar si el servidor esta listo y tiene ssl grade
			if strings.ToLower(serverItem.StatusMessage) == "ready" {
				serverVar.SslGrade = serverItem.Grade
			} else {
				serverVar.SslGrade = "-"
			}

			go func(item server.Server) {
				defer wg.Done()

				// Validar si ocurrió un error en alguna gorutina.
				select {
				case <-ctx.Done():
					return // Ocurrió un error en alguna parte, terminar
				default: //El valor por defecto es para evitar el bloqueo
				}
				result, err := whois.GetWhois(item)
				if err != nil {
					http.Error(w, "Ocurrió un error al recuperar el country y el owner de alguno de los servidores del dominio. ", http.StatusInternalServerError)
					cancel()
					return
				}
				serverList = append(serverList, result)

			}(serverVar)
		}

		wg.Wait()

		if domainVar.ID == 0 { // El dominio no existe
			domainVar.Name = domainName
			domainVar.Title = title
			domainVar.Logo = logo
			domainVar.SearchedAt = time.Now()
			domainVar.ServersChanged = false
			domainVar.IsDown = status
			//Se crea el registro del dominio
			err := domainVar.Create()
			if err != nil {
				http.Error(w, "Error al registrar el dominio", http.StatusInternalServerError)
				return
			}

			//Registramos los servers
			for _, server := range serverList {
				server.Domain = domainVar.ID
				server.State = 1 // 1 - state current
				err := server.Create()
				if err != nil {
					http.Error(w, "No fue posible registrar los servidores asociados al dominio", http.StatusInternalServerError)
					return
				}
			}

		} else { // El dominio existe
			// Se asignan los valores del dominio por si alguno se actualizó
			domainVar.Title = title
			domainVar.Logo = logo
			domainVar.SearchedAt = time.Now() //Se va a actualizar la fecha de consulta
			domainVar.IsDown = status

			//Se debe Validar si los servers cambiaron o no
			serverVar := new(server.Server)
			//Primero se consultan los servers almacenados
			previousServerList, err := serverVar.GetByState(domainVar.ID, 1)
			if err != nil {
				http.Error(w, "No fue posible consultar los servidores previos", http.StatusInternalServerError)
				return
			}

			//Los comparamos mandando como parametro primero los current  y luego los almacenados o previous
			isDifferent := serverVar.CompareIsDifferent(serverList, previousServerList)
			domainVar.ServersChanged = isDifferent //Se registra el estado de si cambiaron o no los servers

			//Si son diferentes se deben cambiar de estado los servers almacenados a previous y registrar los nuevos
			if isDifferent {
				domainVar.ServerschangedAt = time.Now() //Los servers cambiaron, por lo que se registra la fecha
				//Se eliminan los servers previous si existen, dado que los current ocuparan este lugar

				err := serverVar.Delete(domainVar.ID, 2)
				if err != nil {
					http.Error(w, "No fue posible realizar una operación de eliminación.", http.StatusInternalServerError)
					return
				}

				errUpdate := serverVar.Update(domainVar.ID, 2)
				if errUpdate != nil {
					http.Error(w, "No fue posible actualizar los servidores asociados al dominio.", http.StatusInternalServerError)
					return
				}

				//Registramos los servers nuevos dado que cambiaron
				for _, server := range serverList {
					server.Domain = domainVar.ID
					server.State = 1 // 1 - state current
					err := server.Create()
					if err != nil {
						http.Error(w, "No fue posible registrar los servidores asociados al dominio", http.StatusInternalServerError)
						return
					}
				}
			}

			//Se actualiza el registro del dominio
			errUpdate := domainVar.Update()
			if errUpdate != nil {
				http.Error(w, "No fue posible actualizar el registro del dominio", http.StatusInternalServerError)
				return
			}
		}

		//Se asignan los servers al dominio para el retorno del json
		domainVar.Servers = serverList
		domainVar.SslGrade = "-"

		amountServers, errSsl := domainVar.ServersCount(1) //state de los servers current = 1
		if errSsl != nil {
			http.Error(w, "No fue posible recuperar la cantidad de servidores del dominio", http.StatusInternalServerError)
			return
		}

		if amountServers > 0{
		_, errSsl := domainVar.GetSslGrade(1) //state de los servers current = 1
		if errSsl != nil {
			http.Error(w, "No fue posible recuperar el ssl grade actual del dominio", http.StatusInternalServerError)
			return
		}
	}

		//Si el estado de los servidores cambió, entonces existe previous_ssl_grade
		if domainVar.ServersChanged {
			domainVar.PreviousSslGrade = "-"
			amountServers, errSsl := domainVar.ServersCount(2) //state de los servers current = 1
			if errSsl != nil {
				http.Error(w, "No fue posible recuperar la cantidad de servidores del dominio", http.StatusInternalServerError)
				return
			}
			if amountServers > 0{
				_, err := domainVar.GetSslGrade(2) //state de los servers previous = 2
				if err != nil {
					http.Error(w, "No fue posible recuperar el ssl grade previo del dominio", http.StatusInternalServerError)
					return
				}
			}
		}

		j, err := json.Marshal(domainVar)
		if err != nil {
			http.Error(w, "No fue posible recuperar la información de análisis SSL para ser procesada de manera adecuada.", http.StatusInternalServerError)
			return
		}
		//Si se llego hasta este punto la salida es Ok - 200
		w.WriteHeader(http.StatusOK)
		// Estableciendo el tipo de contenido del cuerpo de la respuesta.
		w.Header().Set("Content-Type", "application/json")
		// Escribiendo la respuesta, es decir retornar el dominio en formato JSON.
		w.Write(j)

	} else {
		http.Error(w, "No llego un nombre de dominio", http.StatusBadRequest)
	}
}


// GetDomainListHandler permite manejar la peticion a la ruta '/all/' con el método GET.
// Para retornar todos los dominios consultados previamente.
func GetDomainListHandler(w http.ResponseWriter, r *http.Request) {
	// Puntero a una estructura de tipo Domain
	n := new(domain.Domain)
	// Solicitando todos los dominios consultados en la base de datos
	domains, err := n.GetAll()
	if err != nil {
		http.Error(w, "Error al consultar la lista de dominios en la bd", http.StatusNotFound)
		return
	}
	// Conviertiendo el slice de Dominio a formato JSON, retorna un []byte
	j, err := json.Marshal(domains)
	if err != nil {
		http.Error(w, "Error en creacion de json de respuesta lista de dominios", http.StatusBadRequest)
		return
	}
	// Escribiendo el código de respuesta.
	w.WriteHeader(http.StatusOK)
	// Estableciendo el tipo de contenido del cuerpo de la respuesta.
	w.Header().Set("Content-Type", "application/json")
	// Escribiendo la respuesta, es decir nuestro slice de dominios en formato JSON.
	w.Write(j)
}

/*GetSslResponse permite realizar una peticion a api.ssllabs para analizar un dominio,
retornando una estructura de tipo SslResponse por medio de un channel */
func GetSslResponse(domain string) (ssl.SslResponse,error){

	var endpoints ssl.SslResponse

	// Se realiza la petición a ssllabs para obtener el análisis del dominio
	response, err := http.Get("https://api.ssllabs.com/api/v3/analyze?host=" + domain)
	if err != nil {
		
		return endpoints,err
	}
	if response != nil {
		defer response.Body.Close()
	}
	
	body, err := ioutil.ReadAll(response.Body)
	
	if err != nil {
		return endpoints,err
	}
	
	// Conviertiendo el body a una estructura sslresponse
	errjson := json.Unmarshal(body, &endpoints)	
	if errjson != nil || endpoints.Status == "ERROR" || endpoints.Endpoints == nil {
		return endpoints, errors.New("La api retorno un ssl con estatus ERROR por lo que el dominio fue erroneo.")
	}
	
	return endpoints, nil
}
