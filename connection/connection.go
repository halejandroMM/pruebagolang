/*
 *  connection
 *
 *  @author Alejandro Mendoza Moreno <halejandromendozamoreno@gmail.com>
 *
 *  January 2020
 */
package connection

import (
	"database/sql"
	"log"
	_ "github.com/lib/pq" 
)

var db *sql.DB

func GetConnection() *sql.DB {
	if db != nil {
		return db
	}

	// Conexión a la base de datos domain_db
	db, err := sql.Open("postgres", "postgresql://maxroach@localhost:26257/domain_db?sslmode=disable")
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}

	return db
}

//Está función carga la migrations basicas para comenzar a trabajar.
func MakeMigrations() error {
	db := GetConnection()
	q := `
			CREATE TABLE IF NOT EXISTS states (
	        id INTEGER  DEFAULT unique_rowid() PRIMARY KEY,
       		title VARCHAR(64) NULL
		  );
		 
		  INSERT INTO states (id, title) VALUES (1, 'current');
		  INSERT INTO states (id, title) VALUES (2, 'previous');

		  CREATE TABLE IF NOT EXISTS sslgrades (
	        id INTEGER  DEFAULT unique_rowid() PRIMARY KEY,
			   title VARCHAR(64) NOT NULL,
			   value FLOAT NOT NULL
		  );
		  
		  INSERT INTO sslgrades (id, title, value) VALUES (1, 'A+',7.0);
		  INSERT INTO sslgrades (id, title, value) VALUES (2, 'A',6.0);
		  INSERT INTO sslgrades (id, title, value) VALUES (3, 'A-',5.0);
		  INSERT INTO sslgrades (id, title, value) VALUES (4, 'B',4.0);
		  INSERT INTO sslgrades (id, title, value) VALUES (5, 'C',3.0);
		  INSERT INTO sslgrades (id, title, value) VALUES (6, 'D',2.0);
		  INSERT INTO sslgrades (id, title, value) VALUES (7, 'E',1.0);
		  INSERT INTO sslgrades (id, title, value) VALUES (8, 'F',0.0);
		  
		  --Esto es para el caso en que el servidor este en progress y no tenga aún sslgrade
		  INSERT INTO sslgrades (id, title, value) VALUES (9, '-',8.0);

		  CREATE TABLE IF NOT EXISTS domains (
			id INTEGER  DEFAULT unique_rowid() PRIMARY KEY,
			name VARCHAR(64) NOT NULL,
			title STRING NOT NULL,
			logo STRING NOT NULL,
			dateTimeSearch TIMESTAMPTZ DEFAULT NOW(),
			dateTimeLastChangedServers TIMESTAMPTZ,
			serversChanged BOOL,
			isDown BOOL
		  );

		  CREATE TABLE IF NOT EXISTS servers (
			id INTEGER  DEFAULT unique_rowid() PRIMARY KEY,
			domain INT NOT NULL REFERENCES domains (id),
			address VARCHAR(64) NOT NULL,
			sslGrade INT NOT NULL REFERENCES sslgrades (id),
			country VARCHAR(20),
			owner VARCHAR(64),
			state INT NOT NULL REFERENCES states (id)
		  );
		  `

	_, err := db.Exec(q)
	if err != nil {
		return nil
	}
	return nil
}
