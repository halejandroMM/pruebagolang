package main

import (
	"flag"
	"log"
	"net/http"
	"prueba_go/api"
	"prueba_go/connection"
	"sync"

	"github.com/go-chi/chi"
)

type ServerG struct {
	Ip string `json:"ip"`
}

var wg sync.WaitGroup

func main() {

	chir := chi.NewRouter()

	// flag para realizar la creación de las tablas en la base de datos.
	migrate := flag.Bool("migrate", true, "Crea las tablas en la base de datos")
	flag.Parse()

	if *migrate {
		if err := connection.MakeMigrations(); err != nil {
			log.Fatal(err)
		}
	}

	chir.Get("/domain/{domainName}", api.GetDataDomainHandler)
	chir.Get("/all/", api.GetDomainListHandler)

	http.ListenAndServe(":3000", chir)
}