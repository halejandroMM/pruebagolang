/*
 *  server
 *
 *  @author Alejandro Mendoza Moreno <halejandromendozamoreno@gmail.com>
 *
 *  January 2020
 */
package server

import (
	"errors"
	"prueba_go/connection"
)

type Server struct {
	ID       int    `json:"-"`
	Domain   int    `json:"-"`
	Address  string `json:"address"`
	SslGrade string `json:"ssl_grade"`
	Country  string `json:"country"`
	Owner    string `json:"owner"`
	State    int    `json:"-"`
}

//Este método permite crear un server
func (n Server) Create() error {
	// Realizamos la conexión a la base de datos.
	db := connection.GetConnection()

	// Query para insertar los datos en la tabla servers
	q := `INSERT INTO servers (domain, address, sslGrade, country, owner, state)
			VALUES($1, $2, (SELECT id FROM sslgrades WHERE title=$3), $4, $5, $6)`

	// Preparamos la petición para insertar los datos de manera segura
	// y evitar código malicioso.
	stmt, err := db.Prepare(q)
	if err != nil {
		return err
	}
	defer stmt.Close()
	// Ejecutamos la petición pasando los datos correspondientes. El orden
	// es importante, corresponde con los "$#" del string q.
	r, err := stmt.Exec(n.Domain, n.Address, n.SslGrade, n.Country, n.Owner, n.State)
	if err != nil {
		return err
	}

	// Confirmamos que una fila fuera afectada, debido a que insertamos un
	// registro en la tabla. En caso contrario devolvemos un error.
	if i, err := r.RowsAffected(); err != nil || i != 1 {

		return errors.New("ERROR: Se esperaba una fila afectada")
	}
	// Si llegamos a este punto consideramos que todo el proceso fue exitoso
	// y retornamos un nil para confirmar que no existe un error.
	return nil
}

func (n Server) Update(domainFk int, stateFk int) error {
	// Realizamos la conexión a la base de datos.
	db := connection.GetConnection()

	q := `UPDATE servers set state=$1
		WHERE domain=$2`

	// Preparamos la petición para insertar los datos de manera segura
	// y evitar código malicioso.
	stmt, err := db.Prepare(q)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, errExec := stmt.Exec(stateFk, domainFk)
	if errExec != nil {
		return errExec
	}

	return nil
}

//Este metodo retorna un array de servidores por id dominio y state
func (n *Server) GetByState(domainFk int, stateFk int) ([]Server, error) {
	// Realizamos la conexión a la base de datos.
	db := connection.GetConnection()

	// Query para seleccionar los datos en la tabla servers
	q := `SELECT  address, (SELECT title FROM sslgrades  WHERE id=sslGrade) as sslGrade, country, owner
			FROM servers WHERE domain=$1 and state=$2`

	// Ejecutamos la query
	rows, err := db.Query(q, domainFk, stateFk)
	//Si ocurre un error se retorna un slice de tipo Server vacio y el error
	if err != nil {
		return []Server{}, err
	}
	// Cerramos el recurso
	defer rows.Close()

	// Declaramos un slice de servers para que almacene los server que retorne
	// la petición.
	servers := []Server{}
	// El método Next retorna un bool, mientras sea true indicará que existe
	// un valor siguiente para leer.
	for rows.Next() {
		// Escaneamos el valor actual de la fila e insertamos el retorno
		// en los correspondientes campos del server.
		rows.Scan(
			&n.Address, &n.SslGrade, &n.Country, &n.Owner,
		)
		// Añadimos cada server al slice de servers que declaramos antes.
		servers = append(servers, *n)
	}

	return servers, nil
}

//Este método sirve para eliminar los servidores de un id dominio y un state current o previous
func (n Server) Delete(domainFk int, stateFk int) error {
	// Realizamos la conexión a la base de datos.
	db := connection.GetConnection()

	// Query para eliminar los datos en la tabla servers
	q := `DELETE FROM servers
		WHERE domain=$1 and state=$2`
	
	// Preparamos la petición para insertar los datos de manera segura
	// y evitar código malicioso.
	stmt, err := db.Prepare(q)
	if err != nil {
		return err
	}

	//Cerramos el recurso
	defer stmt.Close()

	_, errExec := stmt.Exec(domainFk, stateFk) //Ejecutamos la query
	if errExec != nil {
		return errExec
	}

	return nil
}

/*Este método compara dos array de estructura server para saber si los servidores cambiaron o no
*/
func (n Server) CompareIsDifferent(currentServers []Server, previousServers []Server) bool {

	//Primero se valida la longitud de ambos, si es diferente no es necesario comparar el contenido, 
	//ya son diferentes
	if len(currentServers) != len(previousServers) {
		return true
	}

	//Se crean dos maps para almacenar el contenido de esta manera se pueden recorrer 
	//y validar si son iguales sin importar el orden de los elementos
	currentServersMap := make(map[Server]int)
	previousServersMap := make(map[Server]int)

	//Se llenan los maps
	for _, currentServersElem := range currentServers {
		currentServersMap[currentServersElem]++
	}
	for _, previousServersElem := range previousServers {
		previousServersMap[previousServersElem]++
	}

	//Se recorren y se valida su igualdad
	for currentServersMapKey, currentServersMapVal := range currentServersMap {
		if previousServersMap[currentServersMapKey] != currentServersMapVal {
			return true
		}
	}

	return false
}
