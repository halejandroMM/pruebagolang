/*
 *  whois
 *
 *  @author Alejandro Mendoza Moreno <halejandromendozamoreno@gmail.com>
 *
 *  January 2020
 */
package whois

import (
	"bufio"
	"prueba_go/model/server"
	"strings"

	"github.com/likexian/whois-go"
)

func GetWhois(data server.Server) (server.Server, error) {

	// Variables contador para detener la lectura de la información whois,
	// si ya se tiene el country y el owner
	contCountry := 0
	contOwner := 0

	//Llamado obtención datos whois
	result, err := whois.Whois(data.Address)
	if err != nil {
		return data, err
	}

	//Se escanean los resultados para obtenerl el country y el owner del servidor
	scanner := bufio.NewScanner(strings.NewReader(result))
	for scanner.Scan() && (contCountry < 1 || contOwner < 1) {

		if strings.Contains(scanner.Text(), "ountry:") == true {
			value := strings.Split(scanner.Text(), ":")
			data.Country = value[1]
			contCountry++

		}
		//Dependiendo de los resultados whois, el owner se encuentra en el campo owner o orgname
		if strings.Contains(scanner.Text(), "wner:") == true {
			value := strings.Split(scanner.Text(), ":")
			data.Owner = value[1]
			contOwner++
		}
		if strings.Contains(scanner.Text(), "rgName:") == true {
			value := strings.Split(scanner.Text(), ":")
			data.Owner = value[1]
			contOwner++
		}
	}
	if err := scanner.Err(); err != nil {
		return data, err
	}

	return data, nil
}