/*
 *  ssl
 *
 *  @author Alejandro Mendoza Moreno <halejandromendozamoreno@gmail.com>
 *
 *  January 2020
 */
package ssl

type SslResponse struct {
	Host            string     `json:"host"`
	Port            int        `json:"port"`
	Protocol        string     `json:"protocol"`
	IsPublic        bool       `json:"isPublic"`
	Status          string     `json:"status"`
	StartTime       int64      `json:"startTime"`
	EngineVersion   string     `json:"engineVersion"`
	CriteriaVersion string     `json:"criteriaVersion"`
	Endpoints       []Endpoint `json:"endpoints"`
}

type Endpoint struct {
	IPAddress            string `json:"ipAddress"`
	ServerName           string `json:"serverName"`
	StatusMessage        string `json:"statusMessage"`
	Grade                string `json:"grade,omitempty"`
	GradeTrustIgnored    string `json:"gradeTrustIgnored,omitempty"`
	HasWarnings          bool   `json:"hasWarnings,omitempty"`
	IsExceptional        bool   `json:"isExceptional,omitempty"`
	Progress             int    `json:"progress"`
	Duration             int    `json:"duration,omitempty"`
	Delegation           int    `json:"delegation"`
	StatusDetails        string `json:"statusDetails,omitempty"`
	StatusDetailsMessage string `json:"statusDetailsMessage,omitempty"`
	Eta                  int    `json:"eta,omitempty"`
}
