/*
 *  website
 *
 *  @author Alejandro Mendoza Moreno <halejandromendozamoreno@gmail.com>
 *
 *  January 2020
 */
package website

import (
	"regexp"
	"sync"

	"golang.org/x/net/html"
)

var Pattern string = "^(shortcut|icon|shortcut icon)$"

//Esta función obtiene el titulo del body de un dominio y lo retorna por un channel
func GetTitle(node *html.Node, channel chan string, wg *sync.WaitGroup) {
	defer wg.Done()
	if title, ok := ReadHtmlTitle(node); ok { //Se encontró el title
		channel <- title
	} else { //Fallo en obtener el link
		channel <- ""
	}

}

//Esta función obtiene el logo del body de un dominio y lo retorna por un channel
func GetLogo(node *html.Node, channel chan string, wg *sync.WaitGroup) {
	defer wg.Done()
	if logo, ok := ReadHtmlLogo(node); ok { //Se encontró el logo
		channel <- logo
	} else { //Fallo en obtener el HTML title
		channel <- ""
	}
}

//Esta función valida si un nodo Html es un title
func isTitleElement(n *html.Node) bool {
	return n.Type == html.ElementNode && n.Data == "title"
}

//Esta función valida si un nodo Html es un link
func isLinkElement(n *html.Node) bool {
	return n.Type == html.ElementNode && n.Data == "link"

}

//Esta función valida si un nodo Html link posee en su atributo rel la 
//expresión regular Pattern, la cual permite identificar si el link contiene
//el logo del dominio.
func ReadHtmlLogo(n *html.Node) (string, bool) {

	if isLinkElement(n) {
		for _, a := range n.Attr {
			if a.Key == "rel" {
				match, _ := regexp.MatchString(Pattern, a.Val)
				if match == true {// El link encaja con la expresión por lo que se extrae el logo
					return getAttribute(n, "href"), true
				}
				break
			}
		}
	}

	for c := n.FirstChild; c != nil; c = c.NextSibling { // Recursión para recorrer cada nodo hasta encontrar los links
		result, ok := ReadHtmlLogo(c)
		if ok {
			return result, ok
		}
	}

	return "", false
}

//Esta función permite obtener a partir de un nodo recuper el valor de su atributo
// ingresado como parametro Key
func getAttribute(n *html.Node, key string) string {

	if n == nil {
		return ""
	}

	for _, a := range n.Attr {
		if a.Key == key {
			return a.Val
		}
	}

	return ""

}

//Esta función valida si un nodo Html es de tipo title y retorna su contenido
func ReadHtmlTitle(n *html.Node) (string, bool) {
	if isTitleElement(n) { //Se encuentra el title y se retorna su data
		return n.FirstChild.Data, true
	}

	for c := n.FirstChild; c != nil; c = c.NextSibling {// Recursión para recorrer cada nodo hasta encontrar el title
		result, ok := ReadHtmlTitle(c)
		if ok {
			return result, ok
		}
	}

	return "", false
}
