/*
 *  domain
 *
 *  @author Alejandro Mendoza Moreno <halejandromendozamoreno@gmail.com>
 *
 *  January 2020
 */
package domain

import (
	"errors"
	"log"
	"prueba_go/connection"
	"prueba_go/model/server"
	"time"
)

type Item struct {
	Info string `json:"info"`
}

type ItemList struct {
	List []Item `json:"items"`
}

type DomainResponse struct {
	ID               int       `json:"-"`
	Name             string    `json:"-"`
	Title            string    `json:"title"`
	Logo             string    `json:"logo"`
	SearchedAt       time.Time `json:"-"`
	ServerschangedAt time.Time `json:"-"`
	ServersChanged   bool      `json:"servers_changed"`
	IsDown           bool      `json:"is_down"`
}

type Domain struct {
	ID               int             `json:"-"`
	Name             string          `json:"-"`
	SearchedAt       time.Time       `json:"-"`
	ServerschangedAt time.Time       `json:"-"`
	Servers          []server.Server `json:"servers"`
	ServersChanged   bool            `json:"servers_changed"`
	SslGrade         string          `json:"ssl_grade"`
	PreviousSslGrade string          `json:"previous_ssl_grade"`
	Logo             string          `json:"logo"`
	Title            string          `json:"title"`
	IsDown           bool            `json:"is_down"`
}

//Este método permite crear un dominio
func (n *Domain) Create() error {

	// Realizamos la conexión a la base de datos.
	db := connection.GetConnection()

	// Query para insertar los datos en la tabla domains
	q := `INSERT INTO domains (name, title, logo, dateTimeSearch, serversChanged,
		isDown) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id`

	// Preparamos la petición para insertar los datos de manera segura
	// y evitar código malicioso.
	stmt, err := db.Prepare(q)
	if err != nil {
		return err
	}
	defer stmt.Close()
	// Ejecutamos la petición pasando los datos correspondientes. El orden
	// es importante, corresponde con los "$#" delstring q.
	r, err := stmt.Query(n.Name, n.Title, n.Logo, n.SearchedAt, n.ServersChanged, n.IsDown)
	if err != nil {
		return err
	}

	defer r.Close()

	//Obtenemos el id del registro que se inserto dado que es necesario para usarlo como Fk de los servers
	for r.Next() {
		var id int
		if err := r.Scan(&id); err != nil {
			log.Fatal(err)
		}
		n.ID = id
	}

	// Si llegamos a este punto consideramos que todo el proceso fue exitoso
	// y retornamos un nil para confirmar que no existe un error.
	return nil
}

//Este método permite actualizar un dominio
func (n Domain) Update() error {
	// Realizamos la conexión a la base de datos.
	db := connection.GetConnection()

	// Query para actualizar los datos en la tabla domains
	q := `UPDATE domains set title=$1, logo=$2, dateTimeSearch=$3, dateTimeLastChangedServers=$4,
		serversChanged=$5, isDown=$6
		WHERE id=$7`

	// Preparamos la petición para insertar los datos de manera segura
	// y evitar código malicioso.
	stmt, err := db.Prepare(q)
	if err != nil {
		return err
	}
	// Cerramos el recurso
	defer stmt.Close()

	// Ejecutamos la petición pasando los datos correspondientes. El orden
	// es importante, corresponde con los "$#" delstring q.
	r, err := stmt.Exec(n.Title, n.Logo, n.SearchedAt, n.ServerschangedAt, n.ServersChanged, n.IsDown, n.ID)
	if err != nil {
		return err
	}
	if i, err := r.RowsAffected(); err != nil || i != 1 {
		return errors.New("ERROR: Se esperaba una fila afectada")
	}

	// Si llegamos a este punto consideramos que todo el proceso fue exitoso
	// y retornamos un nil para confirmar que no existe un error.
	return nil
}

//Esté metodo retorna todos los dominios consultados previamente
func (n *Domain) GetAll() (ItemList, error) {
	// Realizamos la conexión a la base de datos.
	db := connection.GetConnection()

	// Query para seleccionar los datos en la tabla domains
	q := `SELECT
	    id, name, title, logo, dateTimeSearch, dateTimeLastChangedServers, serversChanged, isDown
		FROM domains order by dateTimeSearch `

	// Ejecutamos la query
	rows, err := db.Query(q)
	if err != nil {
		return ItemList{}, err
	}
	// Cerramos el recurso
	defer rows.Close()

	// Declaramos un slice de dominios para que almacene los dominios que retorne
	// la petición.
	domains := ItemList{}
	item := Item{}
	// El método Next retorna un bool, mientras sea true indicará que existe
	// un valor siguiente para leer.
	for rows.Next() {
		// Escaneamos el valor actual de la fila e insertamos el retorno
		// en los correspondientes campos de la nota.
		rows.Scan(&n.ID, &n.Name, &n.Title, &n.Logo, &n.SearchedAt, &n.ServerschangedAt, &n.ServersChanged,
			&n.IsDown)
		// Añadimos cada nueva nota al slice de notas que declaramos antes.
		item.Info = n.Name
		domains.List = append(domains.List, item)
	}

	// Si llegamos a este punto consideramos que todo el proceso fue exitoso
	// y retornamos un nil para confirmar que no existe un error.
	return domains, nil
}

//Esté metodo retorna un dominio por name
func (n *Domain) GetByName(domainName string) (Domain, error) {
	// Realizamos la conexión a la base de datos.
	db := connection.GetConnection()
	q := `SELECT
		id, name, title, logo, dateTimeSearch, dateTimeLastChangedServers, serversChanged, isDown
		FROM domains WHERE name=$1`

	// Ejecutamos la petición recibiendo los datos correspondientes. El orden
	// es importante, corresponde con los atributos del string q para que
	// correspondan con el puntero
	err := db.QueryRow(q, domainName).Scan(
		&n.ID, &n.Name, &n.Title, &n.Logo, &n.SearchedAt, &n.ServerschangedAt, &n.ServersChanged, &n.IsDown,
	)

	// Si ocurre un error se retorna una estructura de tipo Domain vacia y el error
	if err != nil {
		return Domain{}, err
	}

	// Si llegamos a este punto consideramos que todo el proceso fue exitoso
	// y retornamos un nil para confirmar que no existe un error.
	return *n, nil
}

//Esté metodo retorna el sslgrade de un dominio a partir de sus servidores tomando el de menor valor y su estado
// si son current o previous
func (n *Domain) GetSslGrade(stateServer int) (Domain, error) {
	var sslGrade string
	// Realizamos la conexión a la base de datos.
	db := connection.GetConnection()
	q := `SELECT
			title 
		FROM
			sslgrades s,
			(SELECT
				DISTINCT (SELECT
					Min(value) AS value 
				FROM
					sslgrades 
				WHERE
					sslgrades.id = s.sslgrade and sslgrades.id <>9) 
			FROM
				servers s 
			WHERE
				s.domain = $1 
				AND s.state = $2
			) d 
		WHERE
			s.value = d.value
		`
	//Ejecutamos la query y recuperamos el sslgrade
	err := db.QueryRow(q, n.ID, stateServer).Scan(
		&sslGrade,
	)
	if err != nil {
		return *n, err
	}

	switch state := stateServer; state { //Dependiendo del state se asigna como sslgrade actual o previo
	case 1: //SslGrade
		n.SslGrade = sslGrade
	case 2: //Previous SslGrade
		n.PreviousSslGrade = sslGrade
	default:
		n.SslGrade = sslGrade
	}

	return *n, nil
}

//Esté metodo retorna la cantidad de servidores por id y state de los servidores - current o previous
func (n *Domain) ServersCount(stateServer int) (int, error) {
	var count int
	// Realizamos la conexión a la base de datos.
	db := connection.GetConnection()
	q := `SELECT
				 COUNT(id)
			FROM
				servers 
			WHERE
				servers.domain = $1 
				AND servers.state = $2
				AND servers.sslgrade <> 9
		`
	//Ejecutamos la query y recuperamos el count
	err := db.QueryRow(q, n.ID, stateServer).Scan(
		&count,
	)
	if err != nil {
		return 0, err
	}

	return count, nil
}
