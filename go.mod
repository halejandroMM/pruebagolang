module prueba_go

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/andybalholm/cascadia v1.1.0 // indirect
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/lib/pq v1.3.0
	github.com/likexian/whois-go v1.3.1
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa
)
